using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Headers;

namespace Web.Pages
{
    public class LoginPageModel : PageModel
    {
        private readonly IUserService _userService;
        private readonly HttpClient _httpClient;
        private readonly IHttpContextAccessor _httpContextAccessor;

        [BindProperty]
        public string Username { get; set; }

        [BindProperty]
        public string Password { get; set; }

        public string ErrorMessage { get; set; }

        public LoginPageModel(IUserService userService, HttpClient httpClient, IHttpContextAccessor httpContextAccessor)
        {
            _userService = userService;
            _httpClient = httpClient;
            _httpContextAccessor = httpContextAccessor;
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnPostAsync()
        {
            string username = Username;
            string password = Password;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                ErrorMessage = "Invalid username or password";
                return Page();
            }

            var content = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "username", username },
                { "password", password }
            });

            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            var response = await _httpClient.PostAsync("https://localhost:7162/api/User/login", content);

            if (response.IsSuccessStatusCode)
            {
                // Lire le contenu de la r�ponse en tant que cha�ne
                var responseContent = await response.Content.ReadAsStringAsync();

                // D�s�rialiser la cha�ne en tant qu'objet User
                var user = JsonConvert.DeserializeObject<User>(responseContent);

                // Stocker les informations dans la session
                HttpContext.Session.SetInt32("UserId", user.Id);
                HttpContext.Session.SetString("Username", user.Username);

                return RedirectToPage("/Home");
            }
            else if (response.StatusCode == HttpStatusCode.NotFound)
            {
                ErrorMessage = "Invalid username or password";
                return Page();
            }
            else
            {
                ErrorMessage = "An error occurred";
                return Page();
            }
        }
    }
}
