using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Web.Pages
{
    public class LicencePageModel : PageModel
    {
        private readonly HttpClient _httpClient;

        public List<Licence> Licences { get; set; }

        public LicencePageModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IActionResult> OnGet()
        {
            var response = await _httpClient.GetAsync("https://localhost:7162/api/licence");
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Licences = JsonConvert.DeserializeObject<List<Licence>>(content);
                return Page();
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return NotFound();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
