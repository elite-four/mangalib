using Domain.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;

namespace Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly HttpClient _httpClient;

        public List<User> Users { get; set; }

        public IndexModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task OnGetAsync()
        {
            var response = await _httpClient.GetAsync("https://localhost:7162/api/user");
            response.EnsureSuccessStatusCode();

            var content = await response.Content.ReadAsStringAsync();
            Users = JsonConvert.DeserializeObject<List<User>>(content);
        }
    }
}
