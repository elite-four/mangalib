using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using System.Text;

namespace Web.Pages
{
    public class VolumePageModel : PageModel
    {
        private readonly HttpClient _httpClient;
        public Licence Licence { get; set; }
        public List<Volume> Volumes { get; set; }
        public VolumePageModel(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IActionResult> OnGet(int licenceId)
        {
            var licenceResponse = await _httpClient.GetAsync($"https://localhost:7162/api/licence/{licenceId}");
            if (!licenceResponse.IsSuccessStatusCode)
            {
                return NotFound();
            }

            var licenceContent = await licenceResponse.Content.ReadAsStringAsync();
            Licence = JsonConvert.DeserializeObject<Licence>(licenceContent);

            var volumeResponse = await _httpClient.GetAsync($"https://localhost:7162/api/licence/{licenceId}/volumes");
            if (volumeResponse.IsSuccessStatusCode)
            {
                var volumeContent = await volumeResponse.Content.ReadAsStringAsync();
                var volumeObject = JsonConvert.DeserializeObject<Volume>(volumeContent);
                Volumes = new List<Volume> { volumeObject };
            }

            return Page();
        }
        public async Task<IActionResult> OnPostAddToCollection(int volumeId)
        {
            var userId = 1;

            var volumeResponse = await _httpClient.GetAsync($"https://localhost:7162/api/volume/{volumeId}");
            if (!volumeResponse.IsSuccessStatusCode)
            {
                return NotFound();
            }

            var owner = new Owner { UserId = userId, VolumeId = volumeId };
            var json = JsonConvert.SerializeObject(owner);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var addToCollectionResponse = await _httpClient.PostAsync("https://localhost:7162/api/user/collection", content);
            if (!addToCollectionResponse.IsSuccessStatusCode)
            {
                return BadRequest();
            }

            return RedirectToPage("/Collection");
        }
    }
}