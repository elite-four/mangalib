using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Web.Pages
{
    public class CollectionPageModel : PageModel
    {
        private readonly IUserService _userService;

        //public List<Volume> Volumes { get; set; }
        public IEnumerable<Volume> Volumes { get; set; }


        public CollectionPageModel(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult OnGet()
        {
            User user = _userService.GetCurrentUser();

            if (user == null)
            {
                return RedirectToPage("/LoginPage");
            }

            Volumes = _userService.GetCollectionVolumes(user.Id);

            return Page();
        }
    }
}
