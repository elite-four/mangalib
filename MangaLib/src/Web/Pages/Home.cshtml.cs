using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Domain.Models;

namespace Web.Pages
{
    public class HomeModel : PageModel
    {
        public int? UserId { get; private set; }
        public string UserName { get; private set; }
        public string Email { get; set; }
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HomeModel(/*IHttpContextAccessor httpContextAccessor*/)
        {
           // _httpContextAccessor = httpContextAccessor;
        }
        public void OnGet()
        {
            UserId = HttpContext.Session.GetInt32("UserId");
            UserName = HttpContext.Session.GetString("Username");
            Email = HttpContext.Session.GetString("Email");

            
        }
    }
}

