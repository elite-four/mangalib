using Application.Services;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Infrastructure.Repositories;
using Infrastructure.Storage;
using Infrastucture.Data;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var connection = builder.Configuration.GetConnectionString("MangaLibDB");

builder.Services.AddDbContext<IApplicationStorage, AppDbContext>(options =>
{ options.UseSqlServer(connection); });

builder.Services.AddTransient<IUserRepository, UserRepository>();
builder.Services.AddTransient<IUserService, UserService>();

builder.Services.AddTransient<ILicenseRepository, LicenceRepository>();
builder.Services.AddTransient<ILicenceService, LicenceService>();

builder.Services.AddTransient<IVolumeRepository, VolumeRepository>();
builder.Services.AddTransient<IVolumeService, VolumeService>();

builder.Services.AddTransient<IAuthorRepository, AuthorRepository>();
builder.Services.AddTransient<IAuthorService, AuthorService>();

builder.Services.AddTransient<IGenderRepository, GenderRepository>();
builder.Services.AddTransient<IGenderService, GenderService>();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHttpClient();
builder.Services.AddRazorPages();

builder.Services.AddSession();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(30);
});

builder.Services.AddHttpContextAccessor();

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.ExpireTimeSpan = TimeSpan.FromMinutes(20);
        options.SlidingExpiration = true;
        options.AccessDeniedPath = "/Forbidden/";
    });

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseSession();
app.UseAuthorization();

app.MapControllers();

app.MapRazorPages();

using (IServiceScope serviceScope = app.Services.CreateScope())
{
    serviceScope.ServiceProvider.GetRequiredService<IApplicationStorage>().Initialize();
}

app.Run();
