﻿using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                User user = _userService.GetById(id);
                if (user != null)
                {
                    return Ok(user);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] User user)
        {
            try
            {
                _userService.Create(user);

                return CreatedAtAction(nameof(GetById), new { id = user.Id }, user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] User updatedUser)
        {
            try
            {
                var existingUser = _userService.GetById(id);
                if (existingUser == null)
                {
                    return NotFound();
                }

                existingUser.Username = updatedUser.Username;
                existingUser.Image = updatedUser.Image;

                _userService.Update(existingUser);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var existingUser = _userService.GetById(id);
                if (existingUser == null)
                {
                    return NotFound();
                }

                _userService.Delete(existingUser);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("login")]
        public IActionResult Login([FromForm] string username, [FromForm] string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return BadRequest("Invalid username or password");
            }

            User user = _userService.Authenticate(username, password);

            if (user != null)
            {
                HttpContext.Session.SetInt32("UserId", user.Id);
                HttpContext.Session.SetString("Username", user.Username);
                HttpContext.Session.SetString("Email", user.Email);
                var userData = new { Id = user.Id, Username = user.Username, Image = user.Image };
                var userDataJson = JsonConvert.SerializeObject(userData);
                Response.Cookies.Append("CurrentUser", userDataJson, new CookieOptions
                {
                    Expires = DateTime.Now.AddDays(1) // Définir la durée de validité du cookie
                });

                return Ok(user);
            }

            return NotFound();
        }

        protected User CurrentUser
        {
            get
            {
                int? userId = HttpContext.Session.GetInt32("UserId");
                if (userId != null)
                {
                    return _userService.GetById((int)userId);
                }
                return null;
            }
        }
    }
}
