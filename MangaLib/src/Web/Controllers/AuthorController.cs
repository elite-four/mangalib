﻿using Application.Services;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Author author = _authorService.GetById(id);
                if (author != null)
                {
                    return Ok(author);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Author>> GetAll()
        {
            var authors = _authorService.GetAll();
            return Ok(authors);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Author newAuthor)
        {
            try
            {
                var createdAuthor = _authorService.Create(newAuthor);

                return CreatedAtAction(nameof(GetById), new { id = createdAuthor.Id }, createdAuthor);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Author updatedAuthor)
        {
            try
            {
                var existingAuthor = _authorService.GetById(id);
                if (existingAuthor == null)
                {
                    return NotFound();
                }

                existingAuthor.Name = updatedAuthor.Name;
                existingAuthor.Firstname = updatedAuthor.Firstname;

                _authorService.Update(existingAuthor);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var existingAuthor = _authorService.GetById(id);
                if (existingAuthor == null)
                {
                    return NotFound();
                }

                _authorService.Delete(existingAuthor);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
