﻿using Application.Services;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GenderController : ControllerBase
    {
        private readonly IGenderService _genderService;

        public GenderController(IGenderService genderService)
        {
            _genderService = genderService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Gender gender = _genderService.GetById(id);
                if (gender != null)
                {
                    return Ok(gender);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Gender>> GetAll()
        {
            var genders = _genderService.GetAll();
            return Ok(genders);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Gender newGender)
        {
            try
            {
                var createdGender = _genderService.Create(newGender);

                return CreatedAtAction(nameof(GetById), new { id = createdGender.Id }, createdGender);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Gender updatedGender)
        {
            try
            {
                var existingGender = _genderService.GetById(id);
                if (existingGender == null)
                {
                    return NotFound();
                }

                existingGender.Name = updatedGender.Name;

                _genderService.Update(existingGender);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var existingGender = _genderService.GetById(id);
                if (existingGender == null)
                {
                    return NotFound();
                }

                _genderService.Delete(existingGender);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
