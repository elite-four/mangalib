﻿using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VolumeController : ControllerBase
    {
        private readonly IVolumeService _volumeService;

        public VolumeController(IVolumeService volumeService)
        {
            _volumeService = volumeService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Volume volume = _volumeService.GetById(id);
                if (volume != null)
                {
                    return Ok(volume);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Volume>> GetAll()
        {
            var volumes = _volumeService.GetAll();
            return Ok(volumes);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Volume newVolume)
        {
            try
            {
                var createdVolume = _volumeService.Create(newVolume);

                return CreatedAtAction(nameof(GetById), new { id = createdVolume.Id }, createdVolume);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Volume updatedVolume)
        {
            try
            {
                var existingVolume = _volumeService.GetById(id);
                if (existingVolume == null)
                {
                    return NotFound();
                }

                existingVolume.Title = updatedVolume.Title;
                existingVolume.Number = updatedVolume.Number;
                existingVolume.Image = updatedVolume.Image;

                _volumeService.Update(existingVolume);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var existingVolume = _volumeService.GetById(id);
                if (existingVolume == null)
                {
                    return NotFound();
                }

                _volumeService.Delete(existingVolume);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
