﻿using Application.Services;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LicenceController : ControllerBase
    {
        private readonly ILicenceService _licenceService;
        private readonly IVolumeService _volumeService;

        public LicenceController(ILicenceService licenceService, IVolumeService volumeService)
        {
            _licenceService = licenceService;
            _volumeService = volumeService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Licence licence = _licenceService.GetById(id);
                if (licence != null)
                {
                    return Ok(licence);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<Licence>> GetAll()
        {
            var licence = _licenceService.GetAll();
            return Ok(licence);
        }

        [HttpGet("{licenceId}/volumes")]
        public IActionResult GetVolumesByLicenceId(int licenceId)
        {
            try
            {
                var volumes = _volumeService.GetById(licenceId);
                if (volumes != null)
                {
                    return Ok(volumes);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Licence newLicence)
        {
            try
            {
                _licenceService.Create(newLicence);

                return CreatedAtAction(nameof(GetById), new { id = newLicence.Id }, newLicence);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Licence updatedLicence)
        {
            try
            {
                var existingLicence = _licenceService.GetById(id);
                if (existingLicence == null)
                {
                    return NotFound();
                }

                existingLicence.Name = updatedLicence.Name;
                existingLicence.Image = updatedLicence.Image;

                _licenceService.Update(existingLicence);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var existingLicence = _licenceService.GetById(id);
                if (existingLicence == null)
                {
                    return NotFound();
                }

                _licenceService.Delete(existingLicence);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
