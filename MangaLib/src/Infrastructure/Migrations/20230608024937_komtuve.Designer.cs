﻿// <auto-generated />
using System;
using Infrastucture.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Infrastructure.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20230608024937_komtuve")]
    partial class komtuve
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.5")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("Domain.Models.Author", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Firstname")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Authors");
                });

            modelBuilder.Entity("Domain.Models.Bookmark", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("VolumeId")
                        .HasColumnType("int");

                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<int>("Page")
                        .HasColumnType("int");

                    b.HasKey("UserId", "VolumeId");

                    b.HasIndex("VolumeId");

                    b.ToTable("Bookmarks");
                });

            modelBuilder.Entity("Domain.Models.Comment", b =>
                {
                    b.Property<int>("Id")
                        .HasColumnType("int");

                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("VolumeId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Published")
                        .HasColumnType("datetime2");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id", "UserId", "VolumeId");

                    b.HasIndex("UserId");

                    b.HasIndex("VolumeId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Domain.Models.Gender", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int?>("LicenceId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("LicenceId");

                    b.ToTable("Genders");
                });

            modelBuilder.Entity("Domain.Models.Licence", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Image")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Licences");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Image = "Images/Licences/one-piece.png",
                            Name = "One Piece"
                        },
                        new
                        {
                            Id = 2,
                            Image = "Images/Licences/naruto.png",
                            Name = "Naruto"
                        },
                        new
                        {
                            Id = 3,
                            Image = "Images/Licences/dragon-ball.png",
                            Name = "Dragon Ball"
                        });
                });

            modelBuilder.Entity("Domain.Models.Link", b =>
                {
                    b.Property<int>("LicenceId")
                        .HasColumnType("int");

                    b.Property<int>("GenderId")
                        .HasColumnType("int");

                    b.HasKey("LicenceId", "GenderId");

                    b.HasIndex("GenderId");

                    b.ToTable("Links");
                });

            modelBuilder.Entity("Domain.Models.Owner", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("VolumeId")
                        .HasColumnType("int");

                    b.HasKey("UserId", "VolumeId");

                    b.HasIndex("VolumeId");

                    b.ToTable("Owners");
                });

            modelBuilder.Entity("Domain.Models.Rate", b =>
                {
                    b.Property<int>("UserId")
                        .HasColumnType("int");

                    b.Property<int>("VolumeId")
                        .HasColumnType("int");

                    b.Property<int>("Value")
                        .HasColumnType("int");

                    b.HasKey("UserId", "VolumeId");

                    b.HasIndex("VolumeId");

                    b.ToTable("Rates");
                });

            modelBuilder.Entity("Domain.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Image")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Domain.Models.Volume", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Image")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("IsRead")
                        .HasColumnType("bit");

                    b.Property<int>("LicenceId")
                        .HasColumnType("int");

                    b.Property<int>("Number")
                        .HasColumnType("int");

                    b.Property<int>("Pages")
                        .HasColumnType("int");

                    b.Property<DateTime>("Released")
                        .HasColumnType("datetime2");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("LicenceId");

                    b.ToTable("Volumes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Image = "/path/to/volume1.jpg",
                            IsRead = false,
                            LicenceId = 1,
                            Number = 1,
                            Pages = 200,
                            Released = new DateTime(1997, 12, 24, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "One Piece Volume 1"
                        },
                        new
                        {
                            Id = 2,
                            Image = "/path/to/volume2.jpg",
                            IsRead = false,
                            LicenceId = 1,
                            Number = 2,
                            Pages = 200,
                            Released = new DateTime(1998, 1, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "One Piece Volume 2"
                        });
                });

            modelBuilder.Entity("Domain.Models.Writer", b =>
                {
                    b.Property<int>("VolumeId")
                        .HasColumnType("int");

                    b.Property<int>("AuthorId")
                        .HasColumnType("int");

                    b.HasKey("VolumeId", "AuthorId");

                    b.HasIndex("AuthorId");

                    b.ToTable("Writers");
                });

            modelBuilder.Entity("Domain.Models.Bookmark", b =>
                {
                    b.HasOne("Domain.Models.User", "User")
                        .WithMany("Bookmarks")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Volume", "Volume")
                        .WithMany()
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");

                    b.Navigation("Volume");
                });

            modelBuilder.Entity("Domain.Models.Comment", b =>
                {
                    b.HasOne("Domain.Models.User", "User")
                        .WithMany("Comments")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Volume", "Volume")
                        .WithMany("Comments")
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");

                    b.Navigation("Volume");
                });

            modelBuilder.Entity("Domain.Models.Gender", b =>
                {
                    b.HasOne("Domain.Models.Licence", null)
                        .WithMany("Genders")
                        .HasForeignKey("LicenceId");
                });

            modelBuilder.Entity("Domain.Models.Link", b =>
                {
                    b.HasOne("Domain.Models.Gender", "Gender")
                        .WithMany("Links")
                        .HasForeignKey("GenderId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Licence", "Licence")
                        .WithMany()
                        .HasForeignKey("LicenceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Gender");

                    b.Navigation("Licence");
                });

            modelBuilder.Entity("Domain.Models.Owner", b =>
                {
                    b.HasOne("Domain.Models.User", "User")
                        .WithMany("Owners")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Volume", "Volume")
                        .WithMany("Owners")
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");

                    b.Navigation("Volume");
                });

            modelBuilder.Entity("Domain.Models.Rate", b =>
                {
                    b.HasOne("Domain.Models.User", "User")
                        .WithMany("Rates")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Volume", "Volume")
                        .WithMany("Rates")
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");

                    b.Navigation("Volume");
                });

            modelBuilder.Entity("Domain.Models.Volume", b =>
                {
                    b.HasOne("Domain.Models.Licence", "Licence")
                        .WithMany("Volumes")
                        .HasForeignKey("LicenceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Licence");
                });

            modelBuilder.Entity("Domain.Models.Writer", b =>
                {
                    b.HasOne("Domain.Models.Author", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Domain.Models.Volume", "Volume")
                        .WithMany("Writers")
                        .HasForeignKey("VolumeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Volume");
                });

            modelBuilder.Entity("Domain.Models.Gender", b =>
                {
                    b.Navigation("Links");
                });

            modelBuilder.Entity("Domain.Models.Licence", b =>
                {
                    b.Navigation("Genders");

                    b.Navigation("Volumes");
                });

            modelBuilder.Entity("Domain.Models.User", b =>
                {
                    b.Navigation("Bookmarks");

                    b.Navigation("Comments");

                    b.Navigation("Owners");

                    b.Navigation("Rates");
                });

            modelBuilder.Entity("Domain.Models.Volume", b =>
                {
                    b.Navigation("Comments");

                    b.Navigation("Owners");

                    b.Navigation("Rates");

                    b.Navigation("Writers");
                });
#pragma warning restore 612, 618
        }
    }
}
