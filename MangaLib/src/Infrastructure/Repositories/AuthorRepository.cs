﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;

namespace Infrastructure.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly IApplicationStorage _storage;

        public AuthorRepository(IApplicationStorage applicationStorage)
        {
            _storage = applicationStorage;
        }

        public IEnumerable<Author> GetAll()
        {
            return _storage.Authors.ToList();
        }

        public Author GetById(int id)
        {
            return _storage.Authors.FirstOrDefault(a => a.Id == id);
        }

        public Author Create(Author author)
        {
            _storage.Authors.Add(author);
            _storage.SaveChanges();

            return author;
        }

        public Author Update(Author author)
        {
            _storage.SaveChanges();

            return author;
        }

        public Author Delete(Author author)
        {
            _storage.Authors.Remove(author);
            _storage.SaveChanges();

            return author;
        }
    }
}
