﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;

namespace Infrastructure.Repositories
{
    public class VolumeRepository : IVolumeRepository
    {
        private readonly IApplicationStorage _storage;

        public VolumeRepository(IApplicationStorage applicationStorage)
        {
            _storage = applicationStorage;
        }

        public IEnumerable<Volume> GetAll()
        {
            return _storage.Volumes.ToList();
        }

        public Volume GetById(int id)
        {
            return _storage.Volumes.FirstOrDefault(v => v.Id == id);
        }

        public Volume Create(Volume volume)
        {
            _storage.Volumes.Add(volume);
            _storage.SaveChanges();

            return volume;
        }

        public Volume Update(Volume volume)
        {
            _storage.SaveChanges();

            return volume;
        }

        public Volume Delete(Volume volume)
        {
            _storage.Volumes.Remove(volume);
            _storage.SaveChanges();

            return volume;
        }


    }
}
