﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;
using Microsoft.EntityFrameworkCore;


namespace Infrastructure.Repositories;

public class BookmarkRepository : IBookmarkRepository
{
    private readonly IApplicationStorage _storage;

    public BookmarkRepository(IApplicationStorage applicationStorage)
    {
        _storage = applicationStorage;
    }

    public IEnumerable<Bookmark> GetAll()
    {
        return _storage.Bookmarks.Include(b => b.User).Include(b => b.Volume).ToList();
    }

    public Bookmark GetById(int id)
    {
        return _storage.Bookmarks
            .Include(b => b.User)
            .Include(b => b.Volume)
            .FirstOrDefault(b => b.Id == id);
    }

    public Bookmark Create(Bookmark bookmark)
    {
        _storage.Bookmarks.Add(bookmark);
        _storage.SaveChanges();

        return bookmark;
    }

    public Bookmark Update(Bookmark bookmark)
    {
        Bookmark existingBookmark = GetById(bookmark.Id);

        existingBookmark.UserId = bookmark.UserId;
        existingBookmark.VolumeId = bookmark.VolumeId;
        existingBookmark.Page = bookmark.Page;

        _storage.SaveChanges();

        return existingBookmark;
    }

    public Bookmark Delete(Bookmark bookmark)
    {
        Bookmark existingBookmark = GetById(bookmark.Id);

        _storage.Bookmarks.Remove(existingBookmark);
        _storage.SaveChanges();

        return existingBookmark;
    }
}
