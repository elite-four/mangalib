﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;

namespace Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IApplicationStorage _storage;
        public UserRepository(IApplicationStorage applicationStorage) 
        {
            _storage = applicationStorage;
        }
        public IEnumerable<User> GetAll()
        {
            return _storage.Users.ToList();
        }
        public User GetById(int id)
        {
            return _storage.Users.Where(u => u.Id == id).FirstOrDefault();
        }
        public User Create(User user)
        {
            _storage.Users.Add(user);
            _storage.SaveChanges();

            return user;
        }
        public User Update(User user)
        {
            User existingUser = GetById(user.Id);

            existingUser.Username = user.Username;
            existingUser.Image = user.Image;

            _storage.SaveChanges();

            return existingUser;
        }
        public User Delete(User user)
        {
            User existingUser = GetById(user.Id);

            _storage.Users.Remove(existingUser);
            _storage.SaveChanges();

            return existingUser;
        }
    }
}
