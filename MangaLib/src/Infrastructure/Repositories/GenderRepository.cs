﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class GenderRepository : IGenderRepository
    {
        private readonly IApplicationStorage _storage;

        public GenderRepository(IApplicationStorage applicationStorage)
        {
            _storage = applicationStorage;
        }

        public IEnumerable<Gender> GetAll()
        {
            return _storage.Genders.ToList();
        }

        public Gender GetById(int id)
        {
            return _storage.Genders.FirstOrDefault(g => g.Id == id);
        }

        public Gender Create(Gender gender)
        {
            _storage.Genders.Add(gender);
            _storage.SaveChanges();

            return gender;
        }

        public Gender Update(Gender gender)
        {
            _storage.SaveChanges();

            return gender;
        }

        public Gender Delete(Gender gender)
        {
            _storage.Genders.Remove(gender);
            _storage.SaveChanges();

            return gender;
        }
    }
}
