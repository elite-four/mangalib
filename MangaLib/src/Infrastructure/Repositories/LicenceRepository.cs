﻿using Domain.Interfaces.Repositories;
using Domain.Models;
using Infrastructure.Storage;

namespace Infrastructure.Repositories
{
    public class LicenceRepository : ILicenseRepository
    {
        private readonly IApplicationStorage _storage;
        public LicenceRepository(IApplicationStorage applicationStorage)
        {
            _storage = applicationStorage;
        }

        public IEnumerable<Licence> GetAll()
        {
            return _storage.Licences.ToList();
        }

        public Licence? GetById(int id)
        {
            return _storage.Licences.Where(l => l.Id == id).FirstOrDefault();
        }
        public Licence Create(Licence licence)
        {
            _storage.Licences.Add(licence);
            _storage.SaveChanges();

            return licence;
        }
        public Licence Update(Licence licence)
        {
            Licence existingLicence = GetById(licence.Id);

            existingLicence.Name = licence.Name;
            existingLicence.Image = licence.Image;

            _storage.SaveChanges();

            return existingLicence;
        }
        public Licence Delete(Licence licence)
        {
            Licence existingLicence = GetById(licence.Id);

            _storage.Licences.Remove(existingLicence);
            _storage.SaveChanges();

            return existingLicence;
        }
    }
}
