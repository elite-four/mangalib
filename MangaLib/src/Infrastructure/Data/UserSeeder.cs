﻿using Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class UserSeeder : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasData(
                new User
                {
                    Id = 1,
                    Username = "Edward",
                    Email = "edward@fma.com",
                    Password = User.HashPassword("alchemy1"),
                    Image = "/path/to/edward.jpg"
                },
                new User
                {
                    Id = 2,
                    Username = "Alphonse",
                    Email = "alphonse@fma.com",
                    Password = User.HashPassword("alchemy2"),
                    Image = "/path/to/alphonse.jpg"
                },
                new User
                {
                    Id = 3,
                    Username = "Mustang",
                    Email = "mustang@fma.com",
                    Password = User.HashPassword("flameAlchemist"),
                    Image = "/path/to/mustang.jpg"
                },
                new User
                {
                    Id = 4,
                    Username = "Hawkeye",
                    Email = "hawkeye@fma.com",
                    Password = User.HashPassword("sharpshooter"),
                    Image = "/path/to/hawkeye.jpg"
                }
            );
        }
    }
}