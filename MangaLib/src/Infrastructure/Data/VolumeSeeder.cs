﻿using Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class VolumeSeeder : IEntityTypeConfiguration<Volume>
    {
        public void Configure(EntityTypeBuilder<Volume> builder)
        {
            builder.HasData(
                new Volume
                {
                    Id = 1,
                    Number = 1,
                    Title = "One Piece Volume 1",
                    Pages = 200,
                    Released = new DateTime(1997, 12, 24),
                    Image = "/path/to/volume1.jpg",
                    LicenceId = 1
                },
                new Volume
                {
                    Id = 2,
                    Number = 2,
                    Title = "One Piece Volume 2",
                    Pages = 200,
                    Released = new DateTime(1998, 1, 20),
                    Image = "/path/to/volume2.jpg",
                    LicenceId = 1
                }
                // Ajoutez d'autres volumes si nécessaire...
            );
        }
    }
}
