﻿using Domain.Models;
using Infrastructure.Data;
using Infrastructure.Storage;
using Microsoft.EntityFrameworkCore;

namespace Infrastucture.Data
{
    public class AppDbContext : DbContext, IApplicationStorage
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Licence> Licences { get; set; }
        public DbSet<Volume> Volumes { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rate> Rates { get; set; }
        public DbSet<Bookmark> Bookmarks { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Writer> Writers { get; set; }
        public DbSet<Link> Links { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Owner>()
                .HasKey(o => new { o.UserId, o.VolumeId });

            modelBuilder.Entity<Writer>()
                .HasKey(w => new { w.VolumeId, w.AuthorId });

            modelBuilder.Entity<Link>()
                .HasKey(l => new { l.LicenceId, l.GenderId });

            modelBuilder.Entity<Comment>()
                .HasKey(c => new { c.Id, c.UserId, c.VolumeId });

            modelBuilder.Entity<User>()
                .HasMany(u => u.Comments)
                .WithOne(c => c.User);

            modelBuilder.Entity<Volume>()
                .HasMany(v => v.Comments)
                .WithOne(c => c.Volume);

            modelBuilder.Entity<Rate>()
                .HasKey(r => new { r.UserId, r.VolumeId });

            modelBuilder.Entity<Bookmark>()
                .HasKey(b => new { b.UserId, b.VolumeId });

            modelBuilder.ApplyConfiguration(new LicenceSeeder());
            modelBuilder.ApplyConfiguration(new VolumeSeeder());
            modelBuilder.ApplyConfiguration(new UserSeeder());
        }

        public void Initialize()
        {
            Database.Migrate();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await SaveChangesAsync(CancellationToken.None);
        }

        public int SaveChanges()
        {
            return base.SaveChanges();
        }

    }
}
