﻿using Domain.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data
{
    public class LicenceSeeder : IEntityTypeConfiguration<Licence>
    {
        public void Configure(EntityTypeBuilder<Licence> builder)
        {
            builder.HasData(
                new Licence
                {
                    Id = 1,
                    Name = "One Piece",
                    Image = "Images/Licences/one-piece.png"
                },
                new Licence
                {
                    Id = 2,
                    Name = "Naruto",
                    Image = "Images/Licences/naruto.png"
                },
                new Licence
                {
                    Id = 3,
                    Name = "Dragon Ball",
                    Image = "Images/Licences/dragon-ball.png"
                }
            );
        }
    }
}
