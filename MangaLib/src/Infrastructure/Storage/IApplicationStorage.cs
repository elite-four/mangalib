﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Storage
{
    public interface IApplicationStorage
    {
        DbSet<User> Users { get; set; }
        DbSet<Licence> Licences { get; set; }
        DbSet<Volume> Volumes { get; set; }
        DbSet<Author> Authors { get; set; }
        DbSet<Gender> Genders { get; set; }
        DbSet<Owner> Owners { get; set; }
        DbSet<Writer> Writers { get; set; }
        DbSet<Link> Links { get; set; }
        DbSet<Comment> Comments { get; set; }
        DbSet<Rate> Rates { get; set; }
        DbSet<Bookmark> Bookmarks { get; set; }

        void Initialize();

        Task<int> SaveChangesAsync();

        int SaveChanges();

    }
}
