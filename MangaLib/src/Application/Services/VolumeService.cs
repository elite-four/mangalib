﻿namespace Application.Services;

public class VolumeService : IVolumeService
{
    private readonly IVolumeRepository _volumeRepository;

    public VolumeService(IVolumeRepository volumeRepository)
    {
        _volumeRepository = volumeRepository;
    }

    public IEnumerable<Volume> GetAll()
    {
        return _volumeRepository.GetAll();
    }

    public Volume GetById(int id)
    {
        return _volumeRepository.GetById(id);
    }

    public Volume Create(Volume volume)
    {
        return _volumeRepository.Create(volume);
    }

    public Volume Update(Volume volume)
    {
        Volume existingVolume = _volumeRepository.GetById(volume.Id);
        if (existingVolume == null)
        {
            throw new Exception($"Volume with ID {volume.Id} not found.");
        }

        existingVolume.Title = volume.Title;
        existingVolume.Number = volume.Number;
        existingVolume.Image = volume.Image;

        return _volumeRepository.Update(existingVolume);
    }

    public Volume Delete(Volume volume)
    {
        Volume existingVolume = _volumeRepository.GetById(volume.Id);
        if (existingVolume == null)
        {
            throw new Exception($"Volume with ID {volume.Id} not found.");
        }

        return _volumeRepository.Delete(existingVolume);
    }
}
