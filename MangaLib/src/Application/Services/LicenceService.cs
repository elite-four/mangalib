﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Domain.Models;

namespace Application.Services
{
    public class LicenceService : ILicenceService
    {
        private readonly ILicenseRepository _licenceRepository;

        public LicenceService(ILicenseRepository licenseRepository)
        {
            _licenceRepository = licenseRepository;
        }

        public IEnumerable<Licence> GetAll()
        {
            return _licenceRepository.GetAll();
        }

        public Licence GetById(int id)
        {
            return _licenceRepository.GetById(id);
        }

        public Licence Create(Licence licence)
        {
            return _licenceRepository.Create(licence);
        }

        public Licence Update(Licence licence)
        {
            Licence existingLicence = _licenceRepository.GetById(licence.Id);
            if (existingLicence == null)
            {
                throw new Exception($"Licence with ID {licence.Id} not found.");
            }
            existingLicence.Name = licence.Name;
            existingLicence.Image = licence.Image;

            return _licenceRepository.Update(existingLicence);
        }

        public Licence Delete(Licence licence)
        {
            Licence existingLicence = _licenceRepository.GetById(licence.Id);
            if (existingLicence == null)
            {
                throw new Exception($"Licence with ID {licence.Id} not found.");
            }

            return _licenceRepository.Delete(existingLicence);
        }
    }
}
