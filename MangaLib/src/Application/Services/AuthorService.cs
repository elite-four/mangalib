﻿namespace Application.Services;

public class AuthorService : IAuthorService
{
    private readonly IAuthorRepository _authorRepository;

    public AuthorService(IAuthorRepository authorRepository)
    {
        _authorRepository = authorRepository;
    }

    public IEnumerable<Author> GetAll()
    {
        return _authorRepository.GetAll();
    }

    public Author GetById(int id)
    {
        return _authorRepository.GetById(id);
    }

    public Author Create(Author author)
    {
        return _authorRepository.Create(author);
    }

    public Author Update(Author author)
    {
        Author existingAuthor = _authorRepository.GetById(author.Id);
        if (existingAuthor == null)
        {
            throw new Exception($"Author with ID {author.Id} not found.");
        }

        existingAuthor.Name = author.Name;
        existingAuthor.Firstname = author.Firstname;

        return _authorRepository.Update(existingAuthor);
    }

    public Author Delete(Author author)
    {
        Author existingAuthor = _authorRepository.GetById(author.Id);
        if (existingAuthor == null)
        {
            throw new Exception($"Author with ID {author.Id} not found.");
        }

        return _authorRepository.Delete(existingAuthor);
    }
}
