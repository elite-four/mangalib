﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Domain.Models;

namespace Application.Services
{
    public class GenderService : IGenderService
    {
        private readonly IGenderRepository _genderRepository;

        public GenderService(IGenderRepository genderRepository)
        {
            _genderRepository = genderRepository;
        }

        public IEnumerable<Gender> GetAll()
        {
            return _genderRepository.GetAll();
        }

        public Gender GetById(int id)
        {
            return _genderRepository.GetById(id);
        }

        public Gender Create(Gender gender)
        {
            return _genderRepository.Create(gender);
        }

        public Gender Update(Gender gender)
        {
            Gender existingGender = _genderRepository.GetById(gender.Id);
            if (existingGender == null)
            {
                throw new Exception($"Gender with ID {gender.Id} not found.");
            }

            existingGender.Name = gender.Name;

            return _genderRepository.Update(existingGender);
        }

        public Gender Delete(Gender gender)
        {
            Gender existingGender = _genderRepository.GetById(gender.Id);
            if (existingGender == null)
            {
                throw new Exception($"Gender with ID {gender.Id} not found.");
            }

            return _genderRepository.Delete(existingGender);
        }
    }
}
