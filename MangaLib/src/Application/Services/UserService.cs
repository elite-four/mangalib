﻿using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using Domain.Models;
using Microsoft.AspNetCore.Http;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserService(IUserRepository userRepository, IHttpContextAccessor httpContextAccessor)
        {
            _userRepository = userRepository;
            _httpContextAccessor = httpContextAccessor;
        }

        //Use case
        public void AddToCollection(User user, Volume volume)
        {
            Owner owner = new Owner { User = user, Volume = volume };
            user.AddNewOwner(owner);

            _userRepository.Update(user);
        }

        public void RemoveFromCollection(User user, Volume volume)
        {
            user.RemoveAnOwner(user.Owners.Where(o => o.User == user && o.Volume == volume).FirstOrDefault());

            _userRepository.Update(user);
        }

        public User? GetById(int id)
        {
            return _userRepository.GetById(id);
        }

        public User Create(User user)
        {
            return _userRepository.Create(user);
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        public User Update(User user)
        {
            User existingUser = _userRepository.GetById(user.Id);
            if (existingUser == null)
            {
                throw new Exception($"User with ID {user.Id} not found.");
            }

            existingUser.Username = user.Username;
            existingUser.Image = user.Image;

            return _userRepository.Update(existingUser);
        }

        public User Delete(User user)
        {
            User existingUser = _userRepository.GetById(user.Id);
            if (existingUser == null)
            {
                throw new Exception($"User with ID {user.Id} not found.");
            }

            return _userRepository.Delete(existingUser);
        }

        public User Authenticate(string username, string password)
        {
            var user = _userRepository.GetAll().Where(u => u.Username == username).FirstOrDefault();

            if (user == null)
                return null;
            bool isPasswordValid = User.VerifyPassword(password, user.Password);
            if (!isPasswordValid)
                return null;
            return user;
        }

        public User GetCurrentUser()
        {
            var httpContext = _httpContextAccessor.HttpContext;

            var identity = httpContext.User.Identity;
            if (identity.IsAuthenticated)
            {
                var userId = int.Parse(identity.Name);

                var user = _userRepository.GetById(userId);

                return user;
            }

            return null;
        }

        public IEnumerable<Volume> GetCollectionVolumes(int userId)
        {
            var user = _userRepository.GetById(userId);
            if (user == null)
            {
                throw new Exception($"User with ID {userId} not found.");
            }

            return user.Owners.Select(owner => owner.Volume);
        }
    }
}
