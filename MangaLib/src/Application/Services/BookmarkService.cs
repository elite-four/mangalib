﻿namespace Application.Services;

public class BookmarkService : IBookmarkService
{
    private readonly IBookmarkRepository _bookmarkRepository;

    public BookmarkService(IBookmarkRepository bookmarkRepository)
    {
        _bookmarkRepository = bookmarkRepository;
    }

    public IEnumerable<Bookmark> GetAllBookmarksForUser(int userId)
    {
        return _bookmarkRepository.GetAll().Where(b => b.UserId == userId);
    }

    public Bookmark Create(Bookmark bookmark)
    {
        return _bookmarkRepository.Create(bookmark);
    }

    public void Update(Bookmark bookmark, int page)
    {
        if (bookmark == null)
        {
            throw new Exception($"Bookmark with ID {bookmark.Id} not found.");
        }

        bookmark.Page = page;
        _bookmarkRepository.Update(bookmark);
    }

    public void Delete(Bookmark bookmark)
    {
        if (bookmark == null)
        {
            throw new Exception($"Bookmark with ID {bookmark.Id} not found.");
        }

        _bookmarkRepository.Delete(bookmark);
    }
}
