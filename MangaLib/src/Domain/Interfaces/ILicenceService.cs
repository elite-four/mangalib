﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface ILicenceService
    {
        IEnumerable<Licence> GetAll();
        Licence Create(Licence licence);
        Licence Update(Licence licence);
        Licence Delete(Licence licence);
        Licence GetById(int id);
    }
}
