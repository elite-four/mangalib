﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface ILinkRepository
    {
        Link GetById(int id);
        void Add(Link link);
        void Update(Link link);
        void Delete(Link link);
    }
}
