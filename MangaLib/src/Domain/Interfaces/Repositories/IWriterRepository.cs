﻿using Domain.Models;

namespace Domain.Interfaces.Repositories;

public interface IWriterRepository
{
    Writer? GetByVolumeAndAuthorId(int volumeId, int authorId);
    Writer Create(Writer writer);
    Writer Delete(Volume volume, Author author);
}
