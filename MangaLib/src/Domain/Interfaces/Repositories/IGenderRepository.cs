﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface IGenderRepository
    {
        IEnumerable<Gender> GetAll();
        Gender GetById(int id);
        Gender Create(Gender gender);
        Gender Update(Gender gender);
        Gender Delete(Gender gender);
    }
}
