﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface IAuthorRepository
    {
        Author GetById(int id);
        IEnumerable<Author> GetAll();
        Author Create(Author author);
        Author Update(Author author);
        Author Delete(Author author);
    }
}
