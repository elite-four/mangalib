﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface ILicenseRepository
    {
        Licence? GetById(int id);
        Licence Create(Licence license);
        Licence Update(Licence license);
        Licence Delete(Licence license);
        IEnumerable<Licence> GetAll();
    }
}
