﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IRateRepository
    {
        Rate GetById(int id);
        void Add(Rate rate);
        void Update(Rate rate);
        void Delete(Rate rate);
    }
}
