﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IBookmarkRepository
    {
        Bookmark GetById(int id);
        Bookmark Create(Bookmark bookmark);
        Bookmark Update(Bookmark bookmark);
        Bookmark Delete(Bookmark bookmark);
        IEnumerable<Bookmark> GetAll();
    }
}
