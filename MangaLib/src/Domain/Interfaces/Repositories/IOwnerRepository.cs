﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IOwnerRepository
    {
        Owner GetById(int id);
        void Add(Owner owner);
        void Update(Owner owner);
        void Delete(Owner owner);
    }
}
