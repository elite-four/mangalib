﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface IVolumeRepository
    {
        Volume GetById(int id);
        IEnumerable<Volume> GetAll();
        Volume Create(Volume volume);
        Volume Update(Volume volume);
        Volume Delete(Volume volume);
    }
}
