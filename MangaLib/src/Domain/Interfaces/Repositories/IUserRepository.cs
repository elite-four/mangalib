﻿using Domain.Models;

namespace Domain.Interfaces.Repositories
{
    public interface IUserRepository
    {
        User? GetById(int id);
        IEnumerable<User> GetAll();
        User Create(User user);
        User Update(User user);
        User Delete(User user);
    }
}
