﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IUserService
    {
        User GetById(int id);
        IEnumerable<User> GetAll();
        User Create(User user);
        User Update(User user);
        User Delete(User user);
        User Authenticate(string username, string password);
        User GetCurrentUser();
        IEnumerable<Volume> GetCollectionVolumes(int userId);
    }
}
