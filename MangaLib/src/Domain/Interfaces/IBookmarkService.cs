﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IBookmarkService
    {
        IEnumerable<Bookmark> GetAllBookmarksForUser(int userId);
        Bookmark Create(Bookmark bookmark);
        void Update(Bookmark bookmark, int page);
        void Delete(Bookmark bookmark);
    }
}
