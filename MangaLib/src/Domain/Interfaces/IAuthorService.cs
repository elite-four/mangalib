﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IAuthorService
    {
        IEnumerable<Author> GetAll();
        Author GetById(int id);
        Author Create(Author author);
        Author Update(Author author);
        Author Delete(Author author);
    }
}
