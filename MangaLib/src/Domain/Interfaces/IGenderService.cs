﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IGenderService
    {
        IEnumerable<Gender> GetAll();
        Gender GetById(int id);
        Gender Create(Gender gender);
        Gender Update(Gender gender);
        Gender Delete(Gender gender);
    }
}
