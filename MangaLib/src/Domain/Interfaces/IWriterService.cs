﻿using Domain.Models;

namespace Domain.Interfaces;

public interface IWriterService
{
    Writer GetById(int volumeId, int authorId);
    Writer Create(Writer writer);
    Writer Delete(Volume volume, Author author);
}
