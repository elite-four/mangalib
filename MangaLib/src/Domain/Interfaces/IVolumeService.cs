﻿using Domain.Models;

namespace Domain.Interfaces
{
    public interface IVolumeService
    {
        Volume GetById(int id);
        IEnumerable<Volume> GetAll();
        Volume Create(Volume volume);
        Volume Update(Volume volume);
        Volume Delete(Volume volume);
    }
}
