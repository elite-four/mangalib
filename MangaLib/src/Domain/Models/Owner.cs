﻿namespace Domain.Models
{
    public int UserId { get; set; }
    public int VolumeId { get; set; }
    public virtual User User { get; set; }
    public virtual Volume Volume { get; set; }
}
