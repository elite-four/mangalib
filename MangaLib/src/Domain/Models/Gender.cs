﻿namespace Domain.Models
{
    public class Gender
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Link>? Links { get; set; }
    }
}
