﻿namespace Domain.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        private string _password;
        public string Password
        {
            get => _password;
            set => _password = HashPassword(value);
        }

        public string Image { get; set; }
        public virtual ICollection<Owner>? Owners { get; set; }
        public virtual ICollection<Comment>? Comments { get; set; }
        public virtual ICollection<Rate>? Rates { get; set; }
        public virtual ICollection<Bookmark>? Bookmarks { get; set; }

        public static string HashPassword(string password)
        {
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);

            return hashedPassword;
        }

        public static bool VerifyPassword(string password, string hashedPassword)
        {
            return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
        }

        public void AddNewImageProfile(string image)
        {
            Image = image;
        }

        public void AddNewOwner(Owner owner)
        {
            if(CheckIfOwnerAlreadyExists(owner))
            {
                throw new InvalidOperationException("You have already this volume");
            }
            else
            {
                Owners.Add(owner);
            }
        }

        public void RemoveAnOwner(Owner owner)
        {
            if(CheckIfOwnerAlreadyExists(owner))
            {
                Owners.Remove(owner);
            }
            else
            {
                throw new InvalidOperationException("Owner doesnt exist");
            }
        }

        public bool CheckIfOwnerAlreadyExists(Owner owner)
        {
            if(Owners.Contains(owner)) return true;
            else return false;
        }

        public void AddNewCommentToAVolume(Comment comment)
        {
            if(CheckIfCommentAlreadyExists(comment))
            {
                throw new InvalidOperationException();
            }
            else 
            { 
                Comments.Add(comment);
            }
        }

        public void RemoveCommentToAVolume(Comment comment)
        {
            if(!CheckIfCommentAlreadyExists(comment))
            {
                throw new InvalidOperationException("Comment doesnt exist");
            }
            Comments.Remove(comment);
        }

        public bool CheckIfCommentAlreadyExists(Comment comment)
        {
            if(Comments.Contains(comment)) return true; 
            else return false;
        }
    }
}