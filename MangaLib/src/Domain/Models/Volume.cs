﻿namespace Domain.Models
{
    public int Id { get; set; }
    public int Number { get; set; }
    public string Title { get; set; }
    public int Pages { get; set; }
    public DateTime Released { get; set; }
    public string Image { get; set; }
    public bool IsRead { get; set; }
    public int LicenceId { get; set; }
    public virtual Licence Licence { get; set; }
    public virtual ICollection<Owner> Owners { get; set; }
    public virtual ICollection<Writer> Writers { get; set; }
    public virtual ICollection<Comment>? Comments { get; set; }
    public virtual ICollection<Rate>? Rates { get; set; }

    public void MarkAsRead()
    {
        IsRead = true;
    }
}
