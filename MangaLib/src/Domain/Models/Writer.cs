﻿namespace Domain.Models
{
    public class Writer
    {
        public int VolumeId { get; set; }
        public int AuthorId { get; set; }
        public virtual Volume Volume { get; set; }
        public virtual Author Author { get; set; }
    }
}
