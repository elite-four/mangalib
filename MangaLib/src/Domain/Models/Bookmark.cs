﻿namespace Domain.Models;

public class Bookmark
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int VolumeId { get; set; }
    public int Page { get; set; }
    public virtual User User { get; set; }
    public virtual Volume Volume { get; set; }

}
