﻿namespace Domain.Models
{
    public int LicenceId { get; set; }
    public int GenderId { get; set; }
    public virtual Licence Licence { get; set; }
    public virtual Gender Gender { get; set; }
}
