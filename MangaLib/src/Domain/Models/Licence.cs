﻿namespace Domain.Models
{
    public class Licence
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }

        public virtual ICollection<Volume>? Volumes { get; set; }
        public virtual ICollection<Gender>? Genders { get; set; }
    }
}
