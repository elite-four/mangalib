﻿namespace Domain.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int VolumeId { get; set; }
        public DateTime Published { get; set; }
        public string Text { get; set; }
        public virtual User User { get; set; }
        public virtual Volume Volume { get; set; }
    }
}
