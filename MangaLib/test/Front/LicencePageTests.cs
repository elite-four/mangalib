﻿namespace Front
{
    public class LicencePageTests : IDisposable
    {
        private IWebDriver _driver;
        private const string BaseUrl = "https://localhost:7162/";

        public LicencePageTests()
        {
            var options = new ChromeOptions();
            options.AddArgument("--headless");
            _driver = new ChromeDriver(options);
        }

        [Fact]
        public void LicencePage_ShowsDataFromBackend()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Licence");

            var idElement = _driver.FindElement(By.Id("licence-id"));
            var nameElement = _driver.FindElement(By.Id("licence-name"));
            var imageElement = _driver.FindElement(By.Id("licence-image"));

            Assert.Equal("1", idElement.Text);
            Assert.Equal("Licence A", nameElement.Text);
            Assert.Equal("image.jpg", imageElement.GetAttribute("src"));
        }

        [Fact]
        public void PageTitle_ShouldBeLicences()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/licence");

            IWebElement titleElement = _driver.FindElement(By.TagName("h1"));
            string pageTitle = titleElement.Text;

            Assert.Equal("Licences", pageTitle);
        }

        [Fact]
        public void LicencesList_ShouldExist()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Licence");

            bool licencesExist = _driver.FindElements(By.TagName("li")).Count > 0;

            Assert.True(licencesExist);
        }

        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
    }
}