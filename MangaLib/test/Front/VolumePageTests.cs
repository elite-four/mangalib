﻿
using Domain.Models;

namespace Front
{
    public class VolumePageTests : IDisposable
    {
        private IWebDriver _driver;
        private const string BaseUrl = "http://localhost:7162";

        public VolumePageTests()
        {
            var options = new ChromeOptions();
            options.AddArgument("--headless");
            _driver = new ChromeDriver(options);
        }

        [Fact]
        public void VolumePage_ShowsDataFromVolumeObject()
        {
            Volume volume = new Volume()
            {
                Id = 1,
                Number = 1,
                Title = "Tome 1",
                Pages = 100,
                Released = new DateTime(2023, 1, 1),
                Image = "volume.jpg",
                IsRead = false,
                LicenceId = 1,
                Licence = new Licence
                {
                    Id = 1,
                    Name = "Licence A",
                    Image = "licence.jpg"
                }
            };

            _driver.Navigate().GoToUrl(BaseUrl + "/Volume");

            var idElement = _driver.FindElement(By.Id("volume-id"));
            var numberElement = _driver.FindElement(By.Id("volume-number"));
            var titleElement = _driver.FindElement(By.Id("volume-title"));
            var pagesElement = _driver.FindElement(By.Id("volume-pages"));
            var releasedElement = _driver.FindElement(By.Id("volume-released"));
            var imageElement = _driver.FindElement(By.Id("volume-image"));
            var licenceIdElement = _driver.FindElement(By.Id("volume-licence-id"));
            var licenceNameElement = _driver.FindElement(By.Id("volume-licence-name"));

            Assert.Equal(volume.Id.ToString(), idElement.Text);
            Assert.Equal(volume.Number.ToString(), numberElement.Text);
            Assert.Equal(volume.Title, titleElement.Text);
            Assert.Equal(volume.Pages.ToString(), pagesElement.Text);
            Assert.Equal(volume.Released.ToShortDateString(), releasedElement.Text);
            Assert.Equal(volume.Image, imageElement.GetAttribute("src"));
            Assert.Equal(volume.LicenceId.ToString(), licenceIdElement.Text);
            Assert.Equal(volume.Licence.Name, licenceNameElement.Text);
        }

        [Fact]
        public void PageTitle_ShouldBeVolumes()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Licence/1/Volume");

            IWebElement titleElement = _driver.FindElement(By.TagName("h1"));
            string pageTitle = titleElement.Text;

            Assert.Equal("Volumes", pageTitle);
        }

        [Fact]
        public void LicenceSection_ShouldExist()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Licence/1/Volume");

            bool licenceExists = _driver.FindElements(By.TagName("h2")).Count > 0;

            Assert.True(licenceExists);
        }

        [Fact]
        public void VolumesList_ShouldExist()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Licence/1/Volume");

            bool volumesExist = _driver.FindElements(By.TagName("li")).Count > 0;

            Assert.True(volumesExist);
        }

        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
        }
    }
}
