namespace Front
{
    public class LoginPageTests : IDisposable
    {
        private IWebDriver _driver;
        private const string BaseUrl = "http://localhost:7162";


        public LoginPageTests()
        {
            var options = new ChromeOptions();
            options.AddArgument("--headless");
            _driver = new ChromeDriver(options);
        }

        public void Dispose()
        {
            _driver.Dispose();
        }

        [Fact]
        public void LoginPage_ValidCredentials_RedirectsToHomePage()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Login");

            var usernameInput = _driver.FindElement(By.Id("username"));
            usernameInput.SendKeys("Skillou");

            var passwordInput = _driver.FindElement(By.Id("password"));
            passwordInput.SendKeys("rayoux67");

            var loginButton = _driver.FindElement(By.CssSelector("button[type='submit']"));
            loginButton.Click();

            Assert.Equal(BaseUrl + "/", _driver.Url);
        }

        [Fact]
        public void LoginPage_InvalidCredentials_DisplaysErrorMessage()
        {
            _driver.Navigate().GoToUrl(BaseUrl + "/Login");

            var usernameInput = _driver.FindElement(By.Id("username"));
            usernameInput.SendKeys("invalidusername");

            var passwordInput = _driver.FindElement(By.Id("password"));
            passwordInput.SendKeys("invalidpassword");

            var loginButton = _driver.FindElement(By.CssSelector("button[type='submit']"));
            loginButton.Click();

            var errorMessage = _driver.FindElement(By.CssSelector("p"));
            Assert.Equal("Invalid username or password.", errorMessage.Text);
        }
    }
}