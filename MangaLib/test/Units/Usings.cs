global using Xunit;
global using Domain.Models;
global using Microsoft.EntityFrameworkCore;
global using Infrastucture.Data;
global using FluentAssertions;
global using Infrastructure.Repositories;
global using Application.Services;
