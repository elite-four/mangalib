namespace Units
{
    public class Sample
    {
        public static User CreateUser()
        {
            User user = new User
            {
                Username = "Skillou",
                Email = "skillou@gmail.com",
                Password = "MonMotDePasse123",
                Image = "path/to/image.png",
                Owners = new List<Owner>(),
                Comments = new List<Comment>(),
                Rates = new List<Rate>(),
                Bookmarks = new List<Bookmark>()
            };
            return user;
        }

        public static Licence CreateLicence()
        {
            Licence licence = new Licence
            {
                Name = "One Piece",
                Image = "random/path/op.png",
                Genders = new List<Gender>(),
                Volumes = new List<Volume>()
            };
            return licence;
        }

        public static Volume CreateVolume()
        {
            Volume volume = new Volume
            {
                Number = 1,
                Title = "Title",
                Pages = 180,
                Image = "Images/Volumes/OnePiece/tome1.png",
                Released = DateTime.Now,
                LicenceId = 1,
                Licence = CreateLicence(),
                Owners = new List<Owner>(),
                Writers = new List<Writer>(),
                Comments = new List<Comment>(),
                Rates = new List<Rate>(),
            };
            return volume;
        }

        public static Owner CreateOwner()
        {
            Owner owner = new Owner
            {
                User = CreateUser(),
                Volume = CreateVolume(),
                UserId = CreateUser().Id,
                VolumeId = CreateVolume().Id
            };
            return owner;
        }

        public static Comment CreateComment()
        {
            Comment comment = new Comment()
            {
                Published = DateTime.Now,
                Text = "Sacré Volume",
                User = CreateUser(),
                Volume = CreateVolume(),
                UserId = CreateUser().Id,
                VolumeId = CreateVolume().Id
            };
            return comment;
        }
        public static Bookmark CreateBookmark()
        {
            Bookmark bookmark = new Bookmark
            {
                UserId = 1,
                VolumeId = 1,
                Page = 25
            };

            return bookmark;
        }

        public static Author CreateAuthor()
        {
            Author author = new Author
            {
                Id = 1,
                Name = "Oda",
                Firstname = "Eiichiro"
            };
            return author;
        }

        public static AppDbContext CreateAppDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase")
                .Options;

            var dbContext = new AppDbContext(options);

            return dbContext;
        }

        
    }
}