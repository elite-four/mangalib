﻿namespace Units
{
    public class UserTests
    {
        private readonly User _user = Sample.CreateUser();
        private readonly Owner _owner = Sample.CreateOwner();
        private readonly Comment _comment = Sample.CreateComment();

        [Fact]
        public void ShouldCreateUser()
        {
            string username = "Skillou";

            Assert.NotNull(_user);
            Assert.Equal(username, _user.Username);
        }

        [Fact]
        public void ShouldHashPasswordWhenCreatingUser()
        {
            string password = "MonMotDePasse123";

            string hashedPassword = User.HashPassword(password);

            Assert.NotEqual(hashedPassword, _user.Password);
        }

        [Fact]
        public void VerifyPassword_CorrectPassword_ReturnsTrue()
        {
            string password = "MonMotDePasse123";
            string hashedPassword = User.HashPassword(password);

            bool isPasswordValid = User.VerifyPassword(password, hashedPassword);

            Assert.True(isPasswordValid);
        }

        [Fact]
        public void VerifyPassword_IncorrectPassword_ReturnsFalse()
        {
            string password = "MonMotDePasse123";
            string incorrectPassword = "MauvaisMotDePasse123";
            string hashedPassword = User.HashPassword(password);

            bool isPasswordValid = User.VerifyPassword(incorrectPassword, hashedPassword);

            Assert.False(isPasswordValid);
        }

        [Fact]
        public void ShouldAddAProfileImage()
        {
            string image = "path/to/the/image/profile.png";
            
            _user.AddNewImageProfile(image);

            _user.Image.Should().Be(image);
        }

        [Fact]
        public void ShouldAddOwnership()
        {
            _user.AddNewOwner(_owner);

            _user.Owners.Should().Contain(_owner);
        }

        [Fact]
        public void ShouldRemoveOwnership()
        {
            _user.AddNewOwner(_owner);
            _user.RemoveAnOwner(_owner);

            _user.Owners.Should().NotContain(_owner);
        }

        [Fact] 
        public void ShouldThrowAnExceptionIfOwnerAlreadyExistWhenAddIt()
        {
            Action act = () =>
            {
                _user.AddNewOwner(_owner);
                _user.AddNewOwner(_owner);
            };

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void ShouldThrowAnExceptionIfOwnerDoesntExistWhenRemoveIt()
        {
            Action act = () =>
            {
                _user.RemoveAnOwner(_owner);
            };

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void ShouldAddACommentToAVolume()
        {
            _user.AddNewCommentToAVolume(_comment);

            _user.Comments.Should().Contain(_comment);
        }

        [Fact]
        public void ShouldThrowAnExceptionIfCommentToAVolumeAlreadyExists()
        {
            Action act = () =>
            {
                _user.AddNewCommentToAVolume(_comment);
                _user.AddNewCommentToAVolume(_comment);
            };

            act.Should().Throw<Exception>();
        }

        [Fact]
        public void ShouldRemoveCommentToAVolume()
        {
            _user.AddNewCommentToAVolume(_comment);
            _user.RemoveCommentToAVolume(_comment);

            _user.Comments.Should().NotContain(_comment);
        }

        [Fact]
        public void ShouldThrowAnExceptionIfCommentDoesntExistWhenRemoveIt()
        {
            Action act = () =>
            {
                _user.RemoveCommentToAVolume(_comment);
            };

            act.Should().Throw<Exception>();
        }
    }
}
