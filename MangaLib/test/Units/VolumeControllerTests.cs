﻿namespace Units
{
    public class VolumeControllerTests
    {
        private readonly AppDbContext _appDbContext;
        private readonly VolumeRepository _volumeRepository;
        private readonly Volume _volume;

        public VolumeControllerTests()
        {
            _appDbContext = Sample.CreateAppDbContext();
            _volumeRepository = new VolumeRepository(_appDbContext);
            _volume = Sample.CreateVolume();

        }

        [Fact]
        public async void VolumeRepository_GetById_ReturnsUser()
        {
            int volumeId = 1;
            VolumeRepository volumeRepository = new VolumeRepository(_appDbContext);
            VolumeService volumeService = new VolumeService(volumeRepository);

            volumeService.Create(_volume);

            var result = volumeRepository.GetById(volumeId);

            result.Should().NotBeNull();
            result.Should().BeOfType<Volume>();
        }

        [Fact]
        public void VolumeRepository_GetAll_ReturnsAllVolumes()
        {
            VolumeRepository volumeRepository = new VolumeRepository(_appDbContext);
            VolumeService volumeService = new VolumeService(volumeRepository);

            Volume volume2 = Sample.CreateVolume();

            volumeService.Create(volume2);

            var result = volumeRepository.GetAll();

            result.Should().NotBeNullOrEmpty();
            result.Should().ContainEquivalentOf(volume2);
        }
    }
}
