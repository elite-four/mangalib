﻿namespace Units
{
    public class AuthorTests
    {
        private readonly Author _author = Sample.CreateAuthor();

        [Fact]
        public void ShouldHaveCorrectId()
        {
            Assert.Equal(1, _author.Id);
        }

        [Fact]
        public void ShouldHaveNonNegativeId()
        {
            Assert.True(_author.Id >= 0);
        }

        [Fact]
        public void ShouldHaveCorrectName()
        {
            Assert.Equal("Oda", _author.Name);
        }

        [Fact]
        public void ShouldHaveNonEmptyName()
        {
            Assert.NotEmpty(_author.Name);
        }

        [Fact]
        public void ShouldHaveCorrectFirstname()
        {
            Assert.Equal("Eiichiro", _author.Firstname);
        }

        [Fact]
        public void ShouldHaveNonEmptyFirstname()
        {
            Assert.NotEmpty(_author.Firstname);
        }

        [Fact]
        public void ShouldHaveCorrectFullName()
        {
            string expectedFullName = "Eiichiro Oda";
            string actualFullName = $"{_author.Firstname} {_author.Name}";

            Assert.Equal(expectedFullName, actualFullName);
        }
    }
}
