﻿namespace Units
{
    public class LicenceControllerTests
    {
        private readonly AppDbContext _appDbContext;
        private readonly LicenceRepository _licenceRepository;
        private readonly Licence _licence;

        public LicenceControllerTests()
        {
            _appDbContext = Sample.CreateAppDbContext();
            _licenceRepository = new LicenceRepository(_appDbContext);
            _licence = Sample.CreateLicence();

        }

        [Fact]
        public async void LicenceRepository_GetById_ReturnsUser()
        {
            int licenceId = 1;
            LicenceRepository licenceRepository = new LicenceRepository(_appDbContext);
            LicenceService licenceService = new LicenceService(licenceRepository);

            licenceService.Create(_licence);

            var result = licenceRepository.GetById(licenceId);

            result.Should().NotBeNull();
            result.Should().BeOfType<Licence>();
        }

        [Fact]
        public void LicenceRepository_GetAll_ReturnsAllLicences()
        {
            LicenceRepository licenceRepository = new LicenceRepository(_appDbContext);
            LicenceService licenceService = new LicenceService(licenceRepository);

            Licence licence2 = Sample.CreateLicence();
            licence2.Id = 3;
            Licence licence3 = Sample.CreateLicence();
            licence3.Id = 4;

            licenceService.Create(licence2);
            licenceService.Create(licence3);

            var result = licenceRepository.GetAll();

            result.Should().NotBeNullOrEmpty();
            result.Should().ContainEquivalentOf(licence2);
            result.Should().ContainEquivalentOf(licence3);
        }
    }
}
