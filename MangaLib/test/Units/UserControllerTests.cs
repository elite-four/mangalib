﻿namespace Units;

public class UserControllerTests
{
    private AppDbContext _appDbContext;
    private readonly UserRepository _userRepository;
    private readonly User _user;

    public UserControllerTests()
    {
        _appDbContext = Sample.CreateAppDbContext();
        _userRepository = new UserRepository(_appDbContext);
        _user = Sample.CreateUser();
    }

    [Fact]
    public async void UserRepository_GetById_ReturnsUser()
    {
        int userId = 1;
        UserRepository userRepository = new UserRepository(_appDbContext);
        UserService userService = new UserService(userRepository);

        userService.Create(_user);

        var result = userRepository.GetById(userId);

        result.Should().NotBeNull();
        result.Should().BeOfType<User>();
    }
}
