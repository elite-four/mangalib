﻿using Domain.Models;

namespace Integrations;

public class BookmarkServiceTests
{
    private readonly AppDbContext _appDbContext = Sample.CreateAppDbContext();
    private readonly Volume _volume = Sample.CreateVolume();
    private readonly Bookmark _bookmark = Sample.CreateBookmark();

    [Fact]
    public void GetAllBookmarksForUserShouldReturnBookmarksForUser()
    {
        int userId = 1;
        var bookmarkRepository = new BookmarkRepository(_appDbContext);
        var bookmarkService = new BookmarkService(bookmarkRepository);

        var actualBookmarks = bookmarkService.GetAllBookmarksForUser(userId);

        Assert.NotNull(actualBookmarks);

    }

    [Fact]
    public void CreateBookmarkShouldReturnCreatedBookmark()
    {
        var bookmarkRepository = new BookmarkRepository(_appDbContext);
        var bookmarkService = new BookmarkService(bookmarkRepository);

        var createdBookmark = bookmarkService.Create(_bookmark);

        Assert.NotNull(createdBookmark);
    }

    [Fact]
    public void UpdateBookmarkShouldUpdateBookmarkPage()
    {
        int newPage = 15;
        var bookmarkRepository = new BookmarkRepository(_appDbContext);
        var bookmarkService = new BookmarkService(bookmarkRepository);

        bookmarkService.Update(_bookmark, newPage);

        var updatedBookmark = bookmarkRepository.GetById(_bookmark.Id);
        Assert.NotNull(updatedBookmark);
        Assert.Equal(newPage, updatedBookmark.Page);
    }

    [Fact]
    public void DeleteBookmarkShouldDeleteBookmark()
    {
        int bookmarkId = 1;
        var bookmarkRepository = new BookmarkRepository(_appDbContext);
        var bookmarkService = new BookmarkService(bookmarkRepository);

        bookmarkService.Delete(_bookmark);
    }
}
