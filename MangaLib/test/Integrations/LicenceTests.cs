﻿namespace Integrations;

public class LicenceTests
{
    private readonly AppDbContext _appDbContext = Sample.CreateAppDbContext();
    private readonly Licence _licence = Sample.CreateLicence();

    [Fact]
    public void ShouldAddLicenceToDatabase()
    {
        Licence lastLicence = _appDbContext.Licences
            .OrderByDescending(l => l.Id)
            .FirstOrDefault();
        LicenceRepository licenceRepository = new LicenceRepository(_appDbContext);
        LicenceService licenceService = new LicenceService(licenceRepository);

        licenceService.Create(_licence);

        Licence lastLicenceCreated = _appDbContext.Licences
            .OrderByDescending(l => l.Id)
            .FirstOrDefault();
        var result = licenceRepository.GetById(lastLicenceCreated.Id);

        result.Should().NotBeNull();
        result.Should().NotBe(lastLicence);
        result.Should().BeOfType<Licence>();
    }

    [Fact]
    public void ShouldRemoveLicenceFromDatabase()
    {
        Licence lastLicence = _appDbContext.Licences.OrderByDescending(l => l.Id).FirstOrDefault();
        int licenceId = lastLicence.Id;
        LicenceRepository licenceRepository = new LicenceRepository(_appDbContext);
        LicenceService licenceService = new LicenceService(licenceRepository);

        licenceService.Delete(lastLicence);

        var result = licenceRepository.GetById(licenceId);

        result.Should().BeNull();
    }
}

