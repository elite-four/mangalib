﻿namespace Integrations;

public class Sample
{
    public static User CreateUser()
    {
        User user = new User
        {
            Username = "Skillou",
            Email = "skillou@gmail.com",
            Password = "MonMotDePasse123",
            Image = "path/to/image.png",
            Owners = new List<Owner>(),
            Comments = new List<Comment>(),
            Rates = new List<Rate>(),
            Bookmarks = new List<Bookmark>()
        };
        return user;
    }

    public static Licence CreateLicence()
    {
        Licence licence = new Licence
        {
            Name = "One Piece",
            Image = "random/path/op.png"
        };
        return licence;
    }

    public static Volume CreateVolume()
    {
        Volume volume = new Volume
        {
            Number = 1,
            Title = "Title",
            Pages = 180,
            Released = DateTime.Now,
            Image = "img.png",
            LicenceId = 1,
            Licence = CreateLicence(),
            Owners = new List<Owner>(),
            Writers = new List<Writer>()

        };
        return volume;
    }

    public static Owner CreateOwner()
    {
        Owner owner = new Owner
        {
            User = CreateUser(),
            Volume = CreateVolume(),
            UserId = CreateUser().Id,
            VolumeId = CreateVolume().Id
        };
        return owner;
    }

    public static Comment CreateComment()
    {
        Comment comment = new Comment()
        {
            Published = DateTime.Now,
            Text = "Sacré Volume",
            User = CreateUser(),
            Volume = CreateVolume(),
            UserId = CreateUser().Id,
            VolumeId = CreateVolume().Id
        };
        return comment;
    }
    public static Bookmark CreateBookmark()
    {
        Bookmark bookmark = new Bookmark
        {
            UserId = 1,
            VolumeId = 1,
            Page = 25
        };

        return bookmark;
    }

    public static Author CreateAuthor()
    {
        Author author = new Author
        {
            Name = "Oda",
            Firstname = "Eiichiro"
        };
        return author;
    }

    public static AppDbContext CreateAppDbContext()
    {
        var connectionString = "data source=localhost,1433;Initial Catalog=Library;user id=sa;password=@Prout!1234;TrustServerCertificate=True;";

        var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>()
            .UseSqlServer(connectionString)
            .Options;

        var dbContext = new AppDbContext(optionsBuilder);

        return dbContext;
    }
}