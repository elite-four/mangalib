namespace Integrations;

public class UserTests
{
    private readonly AppDbContext _appDbContext = Sample.CreateAppDbContext();
    private readonly User _user = Sample.CreateUser();

    [Fact]
    public void ShouldAddUserToDatabase()
    {
        User lastUser = _appDbContext.Users
            .OrderByDescending(u => u.Id)
            .FirstOrDefault();
        
        UserRepository userRepository = new UserRepository(_appDbContext);
        UserService userService = new UserService(userRepository);

        userService.Create(_user);

        User lastUserCreated = _appDbContext.Users
            .OrderByDescending(u => u.Id)
            .FirstOrDefault();

        var result = userRepository.GetById(lastUserCreated.Id);

        result.Should().NotBeNull();
        result.Should().NotBe(lastUser);
        result.Should().BeOfType<User>();

    }

    [Fact]
    public void ShouldRemoveUserFromDatabase()
    {
        User lastUser = _appDbContext.Users
            .OrderByDescending(u => u.Id)
            .FirstOrDefault();
        int userId = lastUser.Id;
        UserRepository userRepository = new UserRepository(_appDbContext);
        UserService userService = new UserService(userRepository);

        userService.Delete(lastUser);

        var result = userRepository.GetById(userId);

        result.Should().BeNull();
    }
}
