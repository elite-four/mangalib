﻿namespace Integrations;

public class AuthorTests
{
    private readonly AppDbContext _appDbContext = Sample.CreateAppDbContext();
    private readonly Author _author = Sample.CreateAuthor();

    [Fact]
    public void ShouldAddAuthorToDatabase()
    {
        Author lastAuthor = _appDbContext.Authors
            .OrderByDescending(a => a.Id)
            .FirstOrDefault();
        AuthorRepository authorRepository = new AuthorRepository(_appDbContext);
        AuthorService authorService = new AuthorService(authorRepository);

        authorService.Create(_author);

        Author lastAuthorCreated = _appDbContext.Authors
            .OrderByDescending(l => l.Id)
            .FirstOrDefault();
        var result = authorRepository.GetById(lastAuthorCreated.Id);

        result.Should().NotBeNull();
        result.Should().NotBe(lastAuthor);
        result.Should().BeOfType<Author>();
    }


    [Fact]
    public void ShouldRemoveAuthorFromDatabase()
    {
        Author lastAuthor = _appDbContext.Authors
            .OrderByDescending(a => a.Id)
            .FirstOrDefault();
        int authorId = lastAuthor.Id;
        AuthorRepository authorRepository = new AuthorRepository(_appDbContext);
        AuthorService authorService = new AuthorService(authorRepository);

        authorService.Delete(lastAuthor);

        var result = authorRepository.GetById(authorId);

        result.Should().BeNull();
    }
}
