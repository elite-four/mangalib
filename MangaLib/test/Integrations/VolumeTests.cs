﻿namespace Integrations;

public class VolumeTests
{
    private readonly AppDbContext _appDbContext = Sample.CreateAppDbContext();
    private readonly Volume _volume = Sample.CreateVolume();
    

    [Fact]
    public void ShouldAddVolumeToDatabase()
    {
        Volume lastVolume = _appDbContext.Volumes
            .OrderByDescending(v => v.Id)
            .FirstOrDefault();

        VolumeRepository volumeRepository = new VolumeRepository(_appDbContext);
        VolumeService volumeService = new VolumeService(volumeRepository);

        volumeService.Create(_volume);

        Volume lastVolumeCreated = _appDbContext.Volumes
            .OrderByDescending(v => v.Id)
            .FirstOrDefault();

        var result = volumeRepository.GetById(lastVolumeCreated.Id);

        result.Should().NotBeNull();
        result.Should().NotBe(lastVolume);
        result.Should().BeOfType<Volume>();
    }

    [Fact]
    public void ShouldRemoveVolumeFromDatabase()
    {
        Volume lastVolume = _appDbContext.Volumes
            .OrderByDescending(v => v.Id)
            .FirstOrDefault();
        int volumeId = lastVolume.Id;
        VolumeRepository volumeRepository = new VolumeRepository(_appDbContext);
        VolumeService volumeService = new VolumeService(volumeRepository);

        volumeService.Delete(lastVolume);

        var result = volumeRepository.GetById(volumeId);

        result.Should().BeNull();
    }
}

