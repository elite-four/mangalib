# MangaLib

Manga Lib est une application web pour gérer votre collection de mangas. L'application permet aux utilisateurs de suivre leur progression dans les séries qu'ils lisent, d'ajouter les mangas qu'ils possèdent à leur collection, de les marquer comme lus ou non lus, et de les noter.

## Fonctionnalités

- Ajouter des mangas à votre collection
- Marquer les mangas comme lus ou non lus
- Suivre votre progression dans les séries que vous lisez
- Noter les mangas que vous avez lus
- Rechercher des mangas par titre, auteur ou catégorie
- Consulter les critiques de mangas laissées par d'autres utilisateurs
- Discuter avec d'autres membres de la communauté

## Technologies utilisées

Manga Lib a été développé en utilisant les technologies suivantes :

---- pour l'interface utilisateur
---- pour la logique côté serveur
---- pour la base de données


## Contribuer

Si vous souhaitez contribuer à Manga Lib, vous pouvez créer une pull request sur notre repository GitHub. Avant de soumettre votre pull request, assurez-vous que votre code est bien testé et que les tests passent avec succès.

## Auteur
Manga Lib a été développé par Skillou , Racturne , Brice , Guillaume.

## Licence
Manga Lib est distribué sous la licence [nom de la licence]. Veuillez consulter le fichier LICENSE.md pour plus d'informations. (pas encore présent)